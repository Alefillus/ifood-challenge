package br.playlist.temperature.ifood.challenge.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
public class CategoryMusicTest {

    @Test
    public void getCategoryPartyWithTemperatureThirtyOne () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("31.0"));

        assertEquals(CategoryMusic.PARTY, categoryMusic);
    }

    @Test
    public void getCategoryPartyWithTemperatureThirtyDotZeroOne () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("30.01"));

        assertEquals(CategoryMusic.PARTY, categoryMusic);
    }


    @Test
    public void getCategoryClassicalWithTemperatureNineDotNine () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("9.9"));

        assertEquals(CategoryMusic.CLASSICAL, categoryMusic);
    }

    @Test
    public void getCategoryRockWithTemperatureTen () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("10"));

        assertEquals(CategoryMusic.ROCK, categoryMusic);
    }

    @Test
    public void getCategoryPopWithTemperatureThirtyDotNine () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("30.000"));

        assertEquals(CategoryMusic.POP, categoryMusic);
    }

    @Test
    public void getCategoryPopWithTemperatureFifteen () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("15.0"));

        assertEquals(CategoryMusic.POP, categoryMusic);
    }

    @Test
    public void getCategoryRockWithTemperatureFifteen () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("14.99"));

        assertEquals(CategoryMusic.ROCK, categoryMusic);
    }

    @Test
    public void dontGetCategoryRockWithTemperatureFifteenDotZeroOne () {
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(Double.valueOf("15.01"));

        assertFalse(CategoryMusic.ROCK.equals(categoryMusic));
    }

}

package br.playlist.temperature.ifood.challenge.service.music.impl;

import br.playlist.temperature.ifood.challenge.dto.spotify.SpotifyResponseAuthorizationDTO;
import br.playlist.temperature.ifood.challenge.model.CategoryMusic;
import br.playlist.temperature.ifood.challenge.model.Playlist;
import br.playlist.temperature.ifood.challenge.service.music.MusicService;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static com.github.benmanes.caffeine.cache.Caffeine.newBuilder;
import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Service
@Log
public class SpotifyMusicServiceImpl implements MusicService {

    private static final int MAXIMUM_SIZE = 1;

    @Value("${spotify.api}")
    private String api;
    private String apiRecommendations;

    @Value("${spotify.apiAuthorization}")
    private String apiAuthorization;

    @Value("${spotify.token}")
    private String token;

    private final RestTemplate restTemplate;

    private HttpEntity<?> entityAuthorization;

    private final LoadingCache<HttpEntity<?>, String> tokenRequest = newBuilder().maximumSize(MAXIMUM_SIZE).expireAfterWrite(59, MINUTES).build(key -> retrieveToken());

    @Autowired
    public SpotifyMusicServiceImpl (RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    public void init() {
        this.apiRecommendations = this.api + "/recommendations?seed_genres={query_genre}&min_popularity={min_popularity}&limit={limit}";
        entityAuthorization = createAuthorization();
    }

    @Override
    @Cacheable("playlists")
    public Playlist findByCategory(CategoryMusic categoryMusic) {
        log.info("Finding musics by category: " + categoryMusic);

        final Playlist playlist = restTemplate
                .exchange(this.apiRecommendations, HttpMethod.GET, new HttpEntity<>(buildHeaders()), Playlist.class, buildQueryParams(categoryMusic))
                .getBody();

        return playlist;
    }

    private MultiValueMap<String, String> buildHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + tokenRequest.get(entityAuthorization));
        return headers;
    }

    private Map<String, Object> buildQueryParams(CategoryMusic categoryMusic) {
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("query_genre", categoryMusic.getName());
        queryParams.put("limit", 15);
        queryParams.put("min_popularity", 50);
        return queryParams;
    }

    private String retrieveToken() {
        SpotifyResponseAuthorizationDTO responseAuthorizationDTO = restTemplate.postForEntity(apiAuthorization, entityAuthorization, SpotifyResponseAuthorizationDTO.class).getBody();
        return responseAuthorizationDTO.getAccessToken();
    }

    private HttpEntity<?> createAuthorization() {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + this.token);
        return new HttpEntity<>(this.buildPayload(), headers);
    }

    private MultiValueMap<String, String> buildPayload () {
        final MultiValueMap<String, String> payload = new LinkedMultiValueMap<>();
        payload.add("grant_type", "client_credentials");

        return payload;
    }
}

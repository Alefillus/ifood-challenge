package br.playlist.temperature.ifood.challenge.model.process;

import br.playlist.temperature.ifood.challenge.model.Playlist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProcessReturn {

    private UUID id;
    private String url;
    private String message;
    private Playlist playlist;
    private ProcessState processState;
}

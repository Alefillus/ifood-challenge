package br.playlist.temperature.ifood.challenge;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.playlist.temperature.ifood.challenge.controller"))
                .build();

    }

    private ApiInfo apiInfo() {
        return new ApiInfo("Suggestion API for playlists by temperature",
                "API that based on the name of the city or coordinates, takes the temperature and informs a playlist that fits in relation to the temperature at the moment.",
                "1.0.0", StringUtils.EMPTY,
                new Contact("Alessandro Oliveira", StringUtils.EMPTY, "alessandro.fillus@gmail.com"),
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                Collections.emptyList());
    }
}

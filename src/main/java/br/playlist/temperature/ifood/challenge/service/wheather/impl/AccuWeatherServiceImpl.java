package br.playlist.temperature.ifood.challenge.service.wheather.impl;

import br.playlist.temperature.ifood.challenge.dto.accuweather.AccuWeatherCityDTO;
import br.playlist.temperature.ifood.challenge.dto.accuweather.AccuWeatherTemperatureDTO;
import br.playlist.temperature.ifood.challenge.exceptions.CannotFindTemperatureException;
import br.playlist.temperature.ifood.challenge.exceptions.CityNotExistsException;
import br.playlist.temperature.ifood.challenge.exceptions.CoordinatesNotExistsException;
import br.playlist.temperature.ifood.challenge.model.accuwheather.Location;
import br.playlist.temperature.ifood.challenge.repository.LocationRepository;
import br.playlist.temperature.ifood.challenge.service.wheather.WeatherService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Service
@Log
public class AccuWeatherServiceImpl implements WeatherService {

    @Value("${accuweather.api}")
    private String api;
    private String apiByCity;
    private String apiByCoordinates;
    private String apiLocations;
    private final ObjectMapper mapper = new ObjectMapper();

    @Value("${accuweather.apikey}")
    private String apikey;

    private RestTemplate restTemplate;
    private LocationRepository locationRepository;

    @Autowired
    public AccuWeatherServiceImpl(RestTemplate restTemplate,
                                  LocationRepository locationRepository) {
        this.restTemplate = restTemplate;
        this.locationRepository = locationRepository;
    }

    @PostConstruct
    public void init () {
        this.apiByCity = this.api + "/locations/v1/cities/search?apikey={apikey}&q={city}";
        this.apiByCoordinates = this.api + "/locations/v1/cities/geoposition/search?apikey={apikey}&q={latitudeLongitude}";
        this.apiLocations = this.api + "/currentconditions/v1/{keyLocation}?apikey={apikey}";
    }

    @Override
    @Cacheable("citysTemperatures")
    public Double findTemperatureByCityName(String name) {
        try {
            log.info("finding by accuweather");
            Location location = locationRepository.getLocationByName(name.toLowerCase());

            if (location != null) {
                return this.requestTemperatureByKeyLocation(location.getKey());
            } else {
                return this.requestCityAfterTemperature(name);
            }
        } catch (HttpClientErrorException | IOException exe) {
            log.log(Level.SEVERE, "Cannot find temperatures for city in accuweather.", exe);
            throw new CannotFindTemperatureException();
        }
    }

    @Override
    @Cacheable("coordinatesTemperatures")
    public Double findTemperatureByCoordinates(Double latitude, Double longitude) {
        try {
            log.info("finding by accuweather");
            Location location = locationRepository.getLocationByLatitudeAndLongitude(latitude, longitude);

            if (location != null) {
                return this.requestTemperatureByKeyLocation(location.getKey());
            } else {
                return this.requestCoodinatesAfterTemperature(latitude, longitude);
            }
        } catch (HttpClientErrorException | IOException exe) {
            log.log(Level.SEVERE, "Cannot find temperatures for coordinates  in accuweather.", exe);
            throw new CannotFindTemperatureException();
        }
    }

    private Double requestCoodinatesAfterTemperature(Double latitude, Double longitude) throws IOException {
        ResponseEntity<String> response = restTemplate.getForEntity(this.apiByCoordinates, String.class, buildQueryParamsByCoordinates(latitude, longitude));
        AccuWeatherCityDTO city = mapper.readValue(response.getBody(), AccuWeatherCityDTO.class);
        if (city == null) {
            throw new CoordinatesNotExistsException();
        }
        if (locationRepository.getLocationByName(city.getName()) == null) {
            saveLocation(city);
        }

        return this.requestTemperatureByKeyLocation(city.getKey());
    }

    private Map<String, Object> buildQueryParamsByCoordinates(Double latitude, Double longitude) {
        StringBuilder builder = new StringBuilder();
        builder.append(latitude.toString());
        builder.append(",");
        builder.append(longitude.toString());

        final Map<String, Object> queyParams = new HashMap<>();
        queyParams.put("latitudeLongitude", builder.toString());
        queyParams.put("apikey", this.apikey);
        return queyParams;
    }

    private Double requestCityAfterTemperature(String name) throws IOException {
        ResponseEntity<String> response = restTemplate.getForEntity(this.apiByCity, String.class, buildQueryParamsByCity(name.toLowerCase().trim()));
        List<AccuWeatherCityDTO> cities = mapper.readValue(response.getBody(), new TypeReference<List<AccuWeatherCityDTO>>(){});
        if (cities.isEmpty()) {
            throw new CityNotExistsException();
        }
        cities.forEach(city -> {
            if (locationRepository.getLocationByName(city.getName()) == null) {
                saveLocation(city);
            }
        });

        return this.requestTemperatureByKeyLocation(cities.get(0).getKey());
    }

    private void saveLocation(AccuWeatherCityDTO city) {
        Location locationSave = Location.builder()
                .name(city.getName())
                .key(city.getKey())
                .latitude(city.getLatitude())
                .longitude(city.getLongitude())
                .build();
        locationRepository.save(locationSave);
    }


    private Double requestTemperatureByKeyLocation(String key) throws IOException {
        ResponseEntity<String> response = restTemplate.getForEntity(this.apiLocations, String.class, buildQueryParamsByLocations(key));
        List<AccuWeatherTemperatureDTO> temperatures = mapper.readValue(response.getBody(), new TypeReference<List<AccuWeatherTemperatureDTO>>(){});
        return temperatures.get(0).getTemperature();
    }

    private Map<String, Object> buildQueryParamsByLocations(String key) {
        final Map<String, Object> queyParams = new HashMap<>();
        queyParams.put("keyLocation", key);
        queyParams.put("apikey", this.apikey);
        return queyParams;
    }

    private Map<String, Object> buildQueryParamsByCity(String city) {
        final Map<String, Object> queyParams = new HashMap<>();
        queyParams.put("city", city);
        queyParams.put("apikey", this.apikey);
        return queyParams;
    }

}

package br.playlist.temperature.ifood.challenge.service.wheather;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
public interface WeatherService {
    static final int LATITUDE_MAX = 90;
    static final int LATITUDE_MIN = -90;
    static final int LONGITUDE_MAX = 180;
    static final int LONGITUDE_MIN = -180;

    Double findTemperatureByCityName(String name);

    Double findTemperatureByCoordinates(Double latitude, Double longitude);

    static boolean isInvalidCoordinates(Double latitude, Double longitude) {
        return isLatitudeInvalid(latitude) || isLongitudeInvalid(longitude);
    }

    static boolean isLongitudeInvalid(Double longitude) {
        return longitude > LONGITUDE_MAX || longitude < LONGITUDE_MIN;
    }

    static boolean isLatitudeInvalid(Double latitude) {
        return latitude > LATITUDE_MAX || latitude < LATITUDE_MIN;
    }
}

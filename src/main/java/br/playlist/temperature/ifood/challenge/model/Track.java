package br.playlist.temperature.ifood.challenge.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.util.List;
import java.util.Map;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Track {
    private String name;
    private int popularity;
    private String url;
    private List<Artist> artists;
    private Album album;

    @JsonProperty("external_urls")
    private void extractUrl(final Map<String, Object> externalUrls) {
        this.url = (String) externalUrls.get("spotify");
    }
}

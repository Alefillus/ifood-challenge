package br.playlist.temperature.ifood.challenge.dto.spotify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpotifyResponseAuthorizationDTO {

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires_in")
    private Long expiresIn;
}

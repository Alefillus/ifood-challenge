package br.playlist.temperature.ifood.challenge;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Ticker;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @author Alessandro Oliveira
 * @since 02/10/2018.
 */
@Configuration
public class CacheConfiguration {

    @Bean
    public CacheManager cacheManager(Ticker ticker) {
        CaffeineCache playlistsCahce = buildCache("playlists", ticker, 60);
        CaffeineCache coordinatesCache = buildCache("coordinatesTemperatures", ticker, 10);
        CaffeineCache citiesCache = buildCache("citysTemperatures", ticker, 10);

        SimpleCacheManager manager = new SimpleCacheManager();
        manager.setCaches(Arrays.asList(playlistsCahce, coordinatesCache, citiesCache));
        return manager;
    }

    private CaffeineCache buildCache(String name, Ticker ticker, int minutesToExpire) {
        return new CaffeineCache(name, Caffeine.newBuilder()
                .expireAfterWrite(minutesToExpire, TimeUnit.MINUTES)
                .maximumSize(100)
                .ticker(ticker)
                .build());
    }

    @Bean
    public Ticker ticker() {
        return Ticker.systemTicker();
    }
}

package br.playlist.temperature.ifood.challenge.model.process;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
public enum ProcessState {
    IN_QUEUE, PROCESSING, PROCESSED, PROCESSING_FAILED
}

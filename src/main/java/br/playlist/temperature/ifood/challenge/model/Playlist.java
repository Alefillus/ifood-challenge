package br.playlist.temperature.ifood.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.util.List;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Playlist {
    private List<Track> tracks;
}

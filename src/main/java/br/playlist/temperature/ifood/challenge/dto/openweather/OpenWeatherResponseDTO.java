package br.playlist.temperature.ifood.challenge.dto.openweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OpenWeatherResponseDTO {
    private String name;
    private Double temperature;

    @JsonProperty("main")
    private void extractTemperature(final Map<String, Object> main) {
        this.temperature = Double.valueOf(main.get("temp").toString());
    }
}

package br.playlist.temperature.ifood.challenge.dto.accuweather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccuWeatherTemperatureDTO {
    private Double temperature;

    @JsonProperty("Temperature")
    private void extractTemperature(final Map<String, Object> temperature) {
        Map<String, Object> metric = (Map<String, Object>) temperature.get("Metric");
        this.temperature = Double.valueOf(metric.get("Value").toString());
    }
}

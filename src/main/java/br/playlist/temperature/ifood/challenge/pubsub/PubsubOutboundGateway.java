package br.playlist.temperature.ifood.challenge.pubsub;

import br.playlist.temperature.ifood.challenge.model.process.ProcessTemperature;
import org.springframework.integration.annotation.MessagingGateway;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
@MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
public interface PubsubOutboundGateway {
    void sendToPubsub(String id);
}

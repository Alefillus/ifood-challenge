package br.playlist.temperature.ifood.challenge.controller;

import br.playlist.temperature.ifood.challenge.model.Playlist;
import br.playlist.temperature.ifood.challenge.model.accuwheather.Location;
import br.playlist.temperature.ifood.challenge.model.process.ProcessReturn;
import br.playlist.temperature.ifood.challenge.repository.LocationRepository;
import br.playlist.temperature.ifood.challenge.service.playlist.PlaylistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Api(value = "Playlist suggestion", tags = "suggestion")
@RestController
@RequestMapping("/v1/suggestion")
@Log
public class SuggestionController {

    private final PlaylistService playlistService;
    private final LocationRepository locationRepository;

    @Autowired
    public SuggestionController(PlaylistService playlistService, LocationRepository locationRepository) {
        this.playlistService = playlistService;
        this.locationRepository = locationRepository;
    }

    @ApiOperation(value = "Returns Playlist based on geographic coordinates", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({ @ApiResponse(code = 200, message = "OK", response = Playlist.class) })
    @GetMapping("city/{name}")
    public Playlist findByName(@PathVariable("name") String name) {
        return playlistService.findByCityName(name);
    }

    @ApiOperation(value = "Returns the Playlist based on the name of a city", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({ @ApiResponse(code = 200, message = "OK", response = Playlist.class) })
    @GetMapping("coordinates/{latitude}/{longitude}")
    public Playlist findByCoordinates (@PathVariable("latitude") Double latitude,
                                       @PathVariable("longitude") Double longitude) {
        return playlistService.findByCoordinates(latitude, longitude);
    }

    @ApiOperation(value = "Endpoint to perform asynchronous search by city name", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({ @ApiResponse(code = 200, message = "OK", response = ProcessReturn.class) })
    @GetMapping("async/city/{name}")
    public ProcessReturn asyncFindByName(@PathVariable("name") String name) {
        return playlistService.publishFindCity(name);
    }

    @ApiOperation(value = "Endpoint to search for an appropriate playlist asynchronously by coordinates", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({ @ApiResponse(code = 200, message = "OK", response = ProcessReturn.class) })
    @GetMapping("async/coordinates/{latitude}/{longitude}")
    public ProcessReturn asyncFindByCoordinates (@PathVariable("latitude") Double latitude,
                                       @PathVariable("longitude") Double longitude) {
        return playlistService.publishFindCoordinates(latitude, longitude);
    }

    @ApiOperation(value = "Endpoint to track the playlist search processing status", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({ @ApiResponse(code = 200, message = "OK", response = ProcessReturn.class) })
    @GetMapping("follow/{id}")
    public ProcessReturn follow (@PathVariable("id") String id) {
        return playlistService.findProcessTemperature(id);
    }

}

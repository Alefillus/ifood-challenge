package br.playlist.temperature.ifood.challenge.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.util.Map;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Album {
    private String name;
    private String url;

    @JsonProperty("external_urls")
    private void extractUrl(final Map<String, Object> externalUrls) {
        this.url = (String) externalUrls.get("spotify");
    }
}

package br.playlist.temperature.ifood.challenge.pubsub;

import br.playlist.temperature.ifood.challenge.service.playlist.PlaylistService;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
@Component
@Log
public class CityListener {

    private final PlaylistService playlistService;

    public CityListener (PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    public boolean processCity(String id) {
        log.info("process " + id);
        return this.playlistService.processFindPlaylist(id);
    }

}

package br.playlist.temperature.ifood.challenge.dto.accuweather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccuWeatherTemperatureResponseDTO {
    private List<AccuWeatherTemperatureDTO> temperatures;
}

package br.playlist.temperature.ifood.challenge.service.wheather.impl;

import br.playlist.temperature.ifood.challenge.dto.openweather.OpenWeatherResponseDTO;
import br.playlist.temperature.ifood.challenge.exceptions.CannotFindTemperatureException;
import br.playlist.temperature.ifood.challenge.exceptions.CityNotExistsException;
import br.playlist.temperature.ifood.challenge.exceptions.CoordinatesNotExistsException;
import br.playlist.temperature.ifood.challenge.service.wheather.WeatherService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Service
@Log
public class OpenWeatherMapServiceImpl implements WeatherService {

    @Value("${openweathermap.api}")
    private String api;
    private String apiByCity;
    private String apiByCoordinates;

    @Value("${openweathermap.apiId}")
    private String apiId;

    private RestTemplate restTemplate;
    private final WeatherService wheatherService;

    @Autowired
    public OpenWeatherMapServiceImpl(RestTemplate restTemplate,
                                     @Qualifier("accuWeatherServiceImpl") WeatherService wheatherService) {
        this.restTemplate = restTemplate;
        this.wheatherService = wheatherService;
    }

    @PostConstruct
    public void init () {
        this.apiByCity = this.api + "/weather?q={city}&appid={apiId}&units=metric";
        this.apiByCoordinates = this.api + "/weather?lat={latitude}&lon={longitude}&appid={apiId}&units=metric";
    }

    @Override
    @Cacheable("citysTemperatures")
    @HystrixCommand(fallbackMethod = "findTemperatureByCityNameFallback")
    public Double findTemperatureByCityName(String name) {
        try {
            log.info("finding by openWeather");
            ResponseEntity<OpenWeatherResponseDTO> response = restTemplate.getForEntity(this.apiByCity, OpenWeatherResponseDTO.class, buildQueryParamsCity(name.toLowerCase()));
            return response.getBody().getTemperature();
        } catch (HttpClientErrorException exe) {
            log.log(Level.SEVERE, "Cannot find temperatures for city.", exe);
            if (exe.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new CityNotExistsException();
            }
            throw new CannotFindTemperatureException();
        }
    }

    public Double findTemperatureByCityNameFallback(String name) {
        return this.wheatherService.findTemperatureByCityName(name);
    }

    @Override
    @Cacheable("coordinatesTemperatures")
    @HystrixCommand(fallbackMethod = "findTemperatureByCoordinatesFallback")
    public Double findTemperatureByCoordinates(Double latitude, Double longitude) {
        try {
            log.info("finding by openWeather");

            ResponseEntity<OpenWeatherResponseDTO> response = restTemplate.getForEntity(this.apiByCoordinates, OpenWeatherResponseDTO.class, buildQueryParamsCoordinates(latitude.doubleValue(), longitude.doubleValue()));
            return response.getBody().getTemperature();
        } catch (HttpClientErrorException exe) {
            log.log(Level.SEVERE, "Cannot find temperatures for coordinates.", exe);
            throw new CannotFindTemperatureException();
        }
    }

    public Double findTemperatureByCoordinatesFallback(Double latitude, Double longitude) {
        return this.wheatherService.findTemperatureByCoordinates(latitude, longitude);
    }


    private Map<String, Object> buildQueryParamsCoordinates(final double latitude, final double longitude) {
        final Map<String, Object> queyParams = new HashMap<>();
        queyParams.put("latitude", latitude);
        queyParams.put("longitude", longitude);
        queyParams.put("apiId", apiId);
        return queyParams;
    }

    private Map<String, Object> buildQueryParamsCity(final String cityName) {
        final Map<String, Object> queyParams = new HashMap<>();
        queyParams.put("city", cityName);
        queyParams.put("apiId", apiId);
        return queyParams;
    }
}

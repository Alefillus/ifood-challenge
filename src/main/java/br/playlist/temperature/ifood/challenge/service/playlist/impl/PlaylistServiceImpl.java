package br.playlist.temperature.ifood.challenge.service.playlist.impl;

import br.playlist.temperature.ifood.challenge.exceptions.CityNotExistsException;
import br.playlist.temperature.ifood.challenge.exceptions.CoordinatesNotExistsException;
import br.playlist.temperature.ifood.challenge.model.CategoryMusic;
import br.playlist.temperature.ifood.challenge.model.Playlist;
import br.playlist.temperature.ifood.challenge.model.process.ProcessReturn;
import br.playlist.temperature.ifood.challenge.model.process.ProcessState;
import br.playlist.temperature.ifood.challenge.model.process.ProcessTemperature;
import br.playlist.temperature.ifood.challenge.pubsub.PubsubOutboundGateway;
import br.playlist.temperature.ifood.challenge.repository.ProcessTemperatureRepository;
import br.playlist.temperature.ifood.challenge.service.music.MusicService;
import br.playlist.temperature.ifood.challenge.service.playlist.PlaylistService;
import br.playlist.temperature.ifood.challenge.service.wheather.WeatherService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.java.Log;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Service
@Log
public class PlaylistServiceImpl implements PlaylistService {

    private static final String HAS_BEEN_STARTED_FOLLOW_THE_URL = " has been started, follow the url.";
    private static final int MAX_ATTEMPS = 5;
    private static final int MORE_ONE = 1;
    private static final int INIT_ATTEMPT = 1;
    private final WeatherService weatherService;
    private final MusicService musicService;
    private final ProcessTemperatureRepository processTemperatureRepository;
    private Gson gson = new Gson();

    @Value("${application.url.follow.process}")
    private String urlFollow;

    @Autowired
    private PubsubOutboundGateway messagingGateway;

    @Autowired
    public PlaylistServiceImpl (@Qualifier("openWeatherMapServiceImpl") WeatherService weatherService,
                                MusicService musicService,
                                ProcessTemperatureRepository processTemperatureRepository) {
        this.weatherService = weatherService;
        this.musicService = musicService;
        this.processTemperatureRepository = processTemperatureRepository;
    }

    @Override
    public Playlist findByCityName(String name)
    {
        if (StringUtils.isEmpty(name)) {
            throw new CityNotExistsException();
        }

        log.info("find playlist by city:" + name);
        Double temperature = this.weatherService.findTemperatureByCityName(name);
        log.info("temperature in city:" + temperature);
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(temperature);
        log.info("category by city:" + name);
        Playlist playlist = musicService.findByCategory(categoryMusic);

        return playlist;
    }

    @Override
    public Playlist findByCoordinates(Double latitude, Double longitude)
    {
        if (WeatherService.isInvalidCoordinates(latitude, longitude)) {
            throw new CoordinatesNotExistsException();
        }

        log.info(String.format("find playlist by coordinates: %s, %s", latitude.toString(), longitude.toString()));
        Double temperature = this.weatherService.findTemperatureByCoordinates(latitude, longitude);
        log.info("temperature in coordinates:" + temperature);
        CategoryMusic categoryMusic = CategoryMusic.byTemperature(temperature);
        log.info("category by coordinates:" + categoryMusic);
        Playlist playlist = musicService.findByCategory(categoryMusic);

        return playlist;
    }

    @Override
    public ProcessReturn publishFindCity(String name) {
        if (StringUtils.isEmpty(name)) {
            throw new CityNotExistsException();
        }

        final ProcessTemperature processTemperature = ProcessTemperature.builder()
                .cityName(name)
                .attempts(INIT_ATTEMPT)
                .processState(ProcessState.IN_QUEUE)
                .build();

        ProcessTemperature processTemperatureSave = processTemperatureRepository.save(processTemperature);

        messagingGateway.sendToPubsub(processTemperatureSave.getId().toString());

        StringBuilder builder = new StringBuilder();
        builder.append("The search for the playlist of the city: ");
        builder.append(name);
        builder.append(HAS_BEEN_STARTED_FOLLOW_THE_URL);


        return buildProcessReturn(processTemperature, processTemperatureSave, builder);
    }

    @Override
    public ProcessReturn publishFindCoordinates(Double latitude, Double longitude) {
        if (WeatherService.isInvalidCoordinates(latitude, longitude)) {
            throw new CoordinatesNotExistsException();
        }

        final ProcessTemperature processTemperature = ProcessTemperature.builder()
                .latitude(latitude)
                .attempts(INIT_ATTEMPT)
                .longitude(longitude)
                .processState(ProcessState.IN_QUEUE)
                .build();

        ProcessTemperature processTemperatureSave = processTemperatureRepository.save(processTemperature);

        messagingGateway.sendToPubsub(processTemperature.getId().toString());

        StringBuilder builder = new StringBuilder();
        builder.append("The search for the playlist by the coordinates:");
        builder.append(latitude.toString());
        builder.append(",");
        builder.append(longitude.toString());
        builder.append(HAS_BEEN_STARTED_FOLLOW_THE_URL);

        return buildProcessReturn(processTemperature, processTemperatureSave, builder);
    }

    @Override
    public boolean processFindPlaylist(String id) {
        log.info("process find playlist by " + id);
        ProcessTemperature processTemperature = this.findAndUpdateStateToProcess(id);
        boolean processSuccess = true;
        try {
            if (processTemperature.getCityName() != null) {
                Playlist playlist = this.findByCityName(processTemperature.getCityName());
                processTemperature.setPlaylist(gson.toJson(playlist));
                processTemperature.setProcessState(ProcessState.PROCESSED);
                processTemperatureRepository.save(processTemperature);
                log.info("processed find playlist by " + id);
            } else {
                Playlist playlist = this.findByCoordinates(processTemperature.getLatitude(), processTemperature.getLongitude());
                processTemperature.setPlaylist(gson.toJson(playlist));
                processTemperature.setProcessState(ProcessState.PROCESSED);
                processTemperatureRepository.save(processTemperature);
                log.info("processed find playlist by " + id);
            }
            return true;
        } catch (Exception exe) {
            processTemperature.setAttempts(processTemperature.getAttempts() + MORE_ONE);
            processTemperature.setProcessState(ProcessState.PROCESSING_FAILED);
            processTemperatureRepository.save(processTemperature);
            log.log(Level.SEVERE, "procces failed find playlist by " + id, exe);
            if (processTemperature.getAttempts() < MAX_ATTEMPS) {
                processSuccess = false;
            }
        } finally {
            return processSuccess;
        }
    }

    @Override
    public ProcessReturn findProcessTemperature(String id) {
        ProcessTemperature processTemperature = processTemperatureRepository.getById(UUID.fromString(id));
        ProcessReturn.ProcessReturnBuilder processReturn = ProcessReturn.builder()
                .id(processTemperature.getId())
                .processState(processTemperature.getProcessState());

        if (ProcessState.PROCESSED.equals(processTemperature.getProcessState())) {
            processReturn.playlist(gson.fromJson(processTemperature.getPlaylist(), Playlist.class));
            processReturn.message("playlist found successfully");
        }

        if (ProcessState.PROCESSING.equals(processTemperature.getProcessState())) {
            processReturn.url(urlFollow);
            processReturn.message("playlist is being searched");
        }

        if (ProcessState.IN_QUEUE.equals(processTemperature.getProcessState())) {
            processReturn.url(urlFollow);
            processReturn.message("the playlist search was calendar");
        }

        if (ProcessState.PROCESSING_FAILED.equals(processTemperature.getProcessState()) && processTemperature.getAttempts() < MAX_ATTEMPS) {
            processReturn.url(urlFollow);
            processReturn.message("An error occurred while trying to fetch the playlist, but let's keep trying");
        }

        if (ProcessState.PROCESSING_FAILED.equals(processTemperature.getProcessState()) && processTemperature.getAttempts() >= MAX_ATTEMPS) {
            processReturn.message("unfortunately we were unable to fetch your playlist, try requesting again");
        }

       return processReturn.build();
    }

    private ProcessTemperature findAndUpdateStateToProcess (String id) {
        ProcessTemperature processTemperature = processTemperatureRepository.getById(UUID.fromString(id));
        processTemperature.setProcessState(ProcessState.PROCESSING);
        return processTemperatureRepository.save(processTemperature);
    }

    private ProcessReturn buildProcessReturn(ProcessTemperature processTemperature, ProcessTemperature processTemperatureSave, StringBuilder builder) {
        return ProcessReturn.builder()
                .id(processTemperatureSave.getId())
                .message(builder.toString())
                .processState(processTemperature.getProcessState())
                .url(urlFollow)
                .build();
    }

}

package br.playlist.temperature.ifood.challenge.repository;

import br.playlist.temperature.ifood.challenge.model.process.ProcessTemperature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
@Repository
public interface ProcessTemperatureRepository extends CrudRepository<ProcessTemperature, UUID> {
    ProcessTemperature getById(UUID id);
}

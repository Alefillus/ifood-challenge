package br.playlist.temperature.ifood.challenge.model;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
public interface ValidateCategory {
    boolean validateByTemperature (Double temperature);
}

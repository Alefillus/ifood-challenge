package br.playlist.temperature.ifood.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "city name not exists.")
public class CityNotExistsException extends RuntimeException {
}

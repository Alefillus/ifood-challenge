package br.playlist.temperature.ifood.challenge.repository;

import br.playlist.temperature.ifood.challenge.model.accuwheather.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Repository
public interface LocationRepository extends CrudRepository<Location, UUID> {
    Location getLocationByName(String name);
    Location getLocationByKey(String key);
    Location getLocationByLatitudeAndLongitude(Double latitude, Double longitude);
}

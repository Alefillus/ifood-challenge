package br.playlist.temperature.ifood.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Alessandro Oliveira
 * @since 02/10/2018.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "coordinates not exists.")
public class CoordinatesNotExistsException extends RuntimeException {
}

package br.playlist.temperature.ifood.challenge.service.playlist;

import br.playlist.temperature.ifood.challenge.model.Playlist;
import br.playlist.temperature.ifood.challenge.model.process.ProcessReturn;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
public interface PlaylistService {
    Playlist findByCityName(final String name);
    Playlist findByCoordinates(final Double latitude, final Double longitude);
    ProcessReturn publishFindCity(String name);
    ProcessReturn publishFindCoordinates(final Double latitude, final Double longitude);
    boolean processFindPlaylist(String id);
    ProcessReturn findProcessTemperature(String id);
}

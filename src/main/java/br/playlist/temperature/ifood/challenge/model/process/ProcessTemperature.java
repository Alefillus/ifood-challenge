package br.playlist.temperature.ifood.challenge.model.process;

import br.playlist.temperature.ifood.challenge.model.Playlist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Alessandro Oliveira
 * @since 03/10/2018.
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProcessTemperature {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)")
    private UUID id;
    private String cityName;
    private Double latitude;
    private Double longitude;
    @Column(name="playlist", columnDefinition="TEXT")
    private String playlist;
    private ProcessState processState;
    private String webhookCallback;
    private int attempts;
}

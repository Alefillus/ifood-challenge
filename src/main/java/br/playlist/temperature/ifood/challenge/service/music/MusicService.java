package br.playlist.temperature.ifood.challenge.service.music;

import br.playlist.temperature.ifood.challenge.model.CategoryMusic;
import br.playlist.temperature.ifood.challenge.model.Playlist;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
public interface MusicService {
    Playlist findByCategory(CategoryMusic categoryMusic);
}

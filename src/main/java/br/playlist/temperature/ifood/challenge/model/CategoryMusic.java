package br.playlist.temperature.ifood.challenge.model;

import lombok.Getter;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@Getter
public enum CategoryMusic implements ValidateCategory {
    CLASSICAL("classical", 9.999D, null) {
        @Override
        public boolean validateByTemperature (Double temperature) {
            if (temperature <= this.getMax()) {
                return true;
            }
            return false;
        };
    },
    PARTY("party", 30.001D, null){
        @Override
        public boolean validateByTemperature (Double temperature) {
            if (temperature >= this.getMax()) {
                return true;
            }
            return false;
        };
    },
    POP("pop", 30.000D, 15.0D){
        @Override
        public boolean validateByTemperature (Double temperature) {
            if (temperature <= this.getMax() && temperature >= this.getMin()) {
                return true;
            }
            return false;
        };
    },
    ROCK("rock", 14.999D, 10.0D){
        @Override
        public boolean validateByTemperature (Double temperature) {
            if (temperature <= this.getMax() && temperature >= this.getMin()) {
                return true;
            }
            return false;
        };
    };

    private String name;
    private Double max;
    private Double min;

    CategoryMusic(String name, Double max, Double min) {
        this.name = name;
        this.max = max;
        this.min = min;
    }

    public static CategoryMusic byTemperature(Double temperature) {
        for(CategoryMusic categorie : CategoryMusic.values()){
            if(categorie.validateByTemperature(temperature)){
                return categorie;
            }
        }
        return CategoryMusic.CLASSICAL;
    }
}
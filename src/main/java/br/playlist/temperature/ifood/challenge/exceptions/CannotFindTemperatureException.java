package br.playlist.temperature.ifood.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Alessandro Oliveira
 * @since 30/09/2018.
 */
@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "cannot find temperature for your city or coordinates.")
public class CannotFindTemperatureException extends RuntimeException {
}

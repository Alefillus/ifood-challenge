package br.playlist.temperature.ifood.challenge.dto.accuweather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Alessandro Oliveira
 * @since 01/10/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccuWeatherCityDTO {
    @JsonProperty("Key")
    private String key;
    @JsonProperty("LocalizedName")
    private String name;
    private Double latitude;
    private Double longitude;

    @JsonProperty("GeoPosition")
    private void extractCoordinates(final Map<String, Object> coordinates) {
        this.latitude = Double.valueOf(coordinates.get("Latitude").toString());
        this.longitude = Double.valueOf(coordinates.get("Longitude").toString());
    }
}

# iFood Backend Advanced Test (This Fork Alessandro Oliveira)

Create a micro-service able to accept RESTful requests receiving as parameter either city name or lat long coordinates and returns a playlist (only track names is fine) suggestion according to the current temperature.

## Business rules

* If temperature (celcius) is above 30 degrees, suggest tracks for party
* In case temperature is above 15 and below 30 degrees, suggest pop music tracks
* If it's a bit chilly (between 10 and 14 degrees), suggest rock music tracks
* Otherwise, if it's freezing outside, suggests classical music tracks 

## Hints

You can make usage of OpenWeatherMaps API (https://openweathermap.org) to fetch temperature data and Spotify (https://developer.spotify.com) to suggest the tracks as part of the playlist.

## Non functional requirements

As this service will be a worldwide success,it must be prepared to be fault tolerant,responsive and resilient.

Use whatever language, tools and frameworks you feel comfortable to. 

Also, briefly elaborate on your solution, architecture details, choice of patterns and frameworks.

## Solution

An api (/api/v1/suggestion/) has been developed, which has five GET methods that can be executed.


* /v1/suggestion/city/{name}: Endpoint to synchronously search the playlist based on the city and its temperature
* /v1/suggestion/coordinates/{latitude}/{longitude}: Endpoint to synchronously search the playlist based on coordinates and its current temperature
* /v1/suggestion/async/city/{name}: Endpoint that fetches the playlist based on the name of the city asynchronously and will return an ID for you to follow the result in the Follow Endpoint. (See the Processing section per row for more details)
* /v1/suggestion/async/coordinates/{latitude}/{longitude}: Endpoint that will get the playlist based on the coordinates and its temperature at the moment in an asynchronous way, will return an ID for you to follow the result in the Follow Endpoint. (See the Processing section per row for more details)
* /v1/suggestion/follow/{id}: Endpoint that returns the status of the process of fetching the playlist at the moment, if successful it will return an item with Playlist name, returns the following attributes: id, url, message, playlist, processState.    

*Note*: The requests to search the playlist based on the category (CLASSICAL, POP, ROCK, PARTY) use the Spotify API and have a 60 minute cache. The requests to get the temperature based on the coordinates and the name of the city go in the OpenWeatherMap API and if they fail go in the AccuWeather API, these requests have a 10 minute cache.

## Processing

When the search for an endpoint with / async is sent it is added to a queue (PubSub) and an event is logged, returning to the client the ID (UUID) of the event being processed in PubSub. Processing is divided into four states:

* IN_QUEUE: This is when the process was sent to queue, but has not yet been started
* PROCESSING: This is when the event is actually being processed.
* PROCESSED: when the event was processed successfully and when performing the follow of the same it returns to the Playlist.
* PROCESSING_FAILED: When the event was processed, an error occurred in the middle of the process. When an error occurs during the process it will retreat up to five times, after the fifth attempt it will stop trying and return a message stating the fault.

## Technologies / Tools Used

* Java 8 And Spring Boot
* Spring Rest Controller (to rest)
* H2 and Spring Boot JPA (to database) // change h2 to postgress in production
* Lombok (decrease boilerplate code)
* Spring Cache and Caffeine (to Cache requests)
* Spring Circuit Breaker With Hystrix (to fallback for temperature search)
* Spring GCP Pubsub (to process asynchronously)
* Swagger and Swagger UI (to docs API)
* Spring Boot Actuator (to monitor the Spring Boot application)
* Spring Boot Security (to authenticate in the Actuator)

## Docs API and RUN

* the documentation with swagger is at: http://localhost:8370/api/swagger-ui.html
* run: *mvn clean install*
* after run: *mvn spring-boot:run*